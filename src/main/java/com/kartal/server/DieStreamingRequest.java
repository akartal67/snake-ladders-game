package com.kartal.server;

import com.kartal.models.Die;
import com.kartal.models.GameState;
import com.kartal.models.Player;
import io.grpc.stub.StreamObserver;

import java.util.concurrent.ThreadLocalRandom;

public class DieStreamingRequest implements StreamObserver<Die> {

    private StreamObserver<GameState> gameStateStreamObserver;
    private Player client;
    private Player server;

    public DieStreamingRequest(StreamObserver<GameState> gameStateStreamObserver, Player client, Player server) {
        this.gameStateStreamObserver = gameStateStreamObserver;
        this.client = client;
        this.server = server;
    }

    @Override
    public void onNext(Die die) {
        client = getNewPositionPlayer(client, die.getValue());
        if(client.getPosition() != 100){
            server = getNewPositionPlayer(server, ThreadLocalRandom.current().nextInt(1,7));
        }

        gameStateStreamObserver.onNext(getGameState());
    }

    private GameState getGameState() {
        return GameState.newBuilder()
                .addPlayer(client)
                .addPlayer(server)
                .build();
    }

    @Override
    public void onError(Throwable throwable) {

    }

    @Override
    public void onCompleted() {

    }

    Player getNewPositionPlayer(Player player, int dieValue){
        int positon = player.getPosition() + dieValue;
        positon = SnakeAndLaddersMap.getPosition(positon);
        if(positon <= 100){
            player = Player.newBuilder()
                    .setPosition(positon)
                    .setName(player.getName())
                    .build();
        }
        return player;
    }

}
