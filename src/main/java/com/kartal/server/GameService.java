package com.kartal.server;

import com.kartal.models.Die;
import com.kartal.models.GameServiceGrpc;
import com.kartal.models.GameState;
import com.kartal.models.Player;
import io.grpc.stub.StreamObserver;

public class GameService extends GameServiceGrpc.GameServiceImplBase {
    @Override
    public StreamObserver<Die> roll(StreamObserver<GameState> responseObserver) {
        Player client = Player.newBuilder().setName("client").setPosition(0).build();
        Player server = Player.newBuilder().setName("server").setPosition(0).build();
        return new DieStreamingRequest(responseObserver, client, server);
    }
}
