package com.kartal.client;

import com.google.common.util.concurrent.Uninterruptibles;
import com.kartal.models.Die;
import com.kartal.models.GameState;
import com.kartal.models.Player;
import io.grpc.stub.StreamObserver;

import javax.swing.*;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class GameStateStreamingResponse implements StreamObserver<GameState> {

    private final CountDownLatch latch;
    private StreamObserver<Die> dieStreamObserver;

    public void setDieStreamObserver(StreamObserver<Die> dieStreamObserver) {
        this.dieStreamObserver = dieStreamObserver;
    }

    public GameStateStreamingResponse(CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    public void onNext(GameState gameState) {
        List<Player> players = gameState.getPlayerList();
        players.forEach(player -> System.out.println(player.getName() + ":" + player.getPosition()));
        boolean isGameOver = players.stream()
                .anyMatch(player -> player.getPosition() == 100);
        if(isGameOver){
            System.out.println("Game over");
            onCompleted();
        }else{
            Uninterruptibles.sleepUninterruptibly(1, TimeUnit.SECONDS);
            roll();
        }
        System.out.println("-------------------------");
    }

    @Override
    public void onError(Throwable throwable) {
        latch.countDown();
    }

    @Override
    public void onCompleted() {
        latch.countDown();
    }

    public void roll(){
        int die = ThreadLocalRandom.current().nextInt(1,7);
        Die dieObj = Die.newBuilder().setValue(die).build();
        dieStreamObserver.onNext(dieObj);
    }

}
