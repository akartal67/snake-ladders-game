package com.kartal.client;

import com.kartal.models.Die;
import com.kartal.models.GameServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.concurrent.CountDownLatch;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GameClientTest {

    private GameServiceGrpc.GameServiceStub stub;

    @BeforeAll
    public void setup(){
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 6565)
                .usePlaintext()
                .build();
        stub = GameServiceGrpc.newStub(channel);
    }

    @Test
    void clientGameTest() throws InterruptedException {
        CountDownLatch count = new CountDownLatch(1);
        GameStateStreamingResponse streamingResponse = new GameStateStreamingResponse(count);
        StreamObserver<Die> dieStreamObserver = stub.roll(streamingResponse);
        streamingResponse.setDieStreamObserver(dieStreamObserver);
        streamingResponse.roll();
        count.await();
    }
}
